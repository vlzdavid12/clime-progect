# Progect Clime App
### Preview Design Adobe Illustrator
![alt text](https://gitlab.com/vlzdavid12/clime-progect/-/raw/master/src/assets/images/screenshot.png)


## Progect Description
This project is created with react.js, CSS,  Sass, Gridlex Framework for Columns, and Adobe Illustreitor.

### Preview Web
![alt text](https://gitlab.com/vlzdavid12/clime-progect/-/raw/master/src/assets/images/screenshot2.png)


In the project directory, you can run:

```
cd clime-app
npm install
npm start
npm run build
```

## Url Develoment

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Api key 
Official website https://openweathermap.org/current

Archive .env 
```
REACT_APP_API_KEY = 0a7f41364fe2404e1530ce7b6f7e13e7
```
## DEMO
Demo App: [Clime App](https://quirky-roentgen-c102da.netlify.app/)


Developer for [David Valenzuela Pardo](https://www.behance.net/gallery/116033373/State-Progect-Clime)
