import React, { useEffect, useState } from 'react';
import { Animated } from "react-animated-css";
import { SliderClime } from './components/sliderClime';
import { CardClimeGeneral } from './components/cardClimeGeneral';
import { CardClime } from './components/cardClime';
import { ClimeDay } from './components/parts/climeDay';
import { AuthorClime } from './components/parts/authorClime';
import { NewsClime } from './components/parts/newsClime';
import imagelocation from './assets/images/location.png';
import "./assets/css/flexgrid.min.css";
import './assets/css/App.scss';
import {requestForesCast} from "./components/helpers/requestHTTP";

function App() {

    const [visible, setVisible] = useState(true);
    const [clime, setClime] = useState()

    const URL = 'https://api.openweathermap.org/data/2.5/find?';
    const token = process.env.REACT_APP_API_KEY

    useEffect(() => {
      requestForesCast(setClime, token, URL)
    }, [setClime, token])

    const handlerClick = () => {
        alert("Wellcome Clime State")
    }

    const handlerMenu = () => {
        if (visible === true) {
            setVisible(false)
        } else {
            setVisible(true);
        }
    }

    if (!clime) {
        return (
            <div className="loader-container">
                <div className="content-stripple">
                <div className="lds-ripple"><div></div><div></div></div>
                <p className="txt-loader">Loader...</p>
                </div>
            </div>
        );
      }

    return (<>
        <div className="container-clime">
            <button onClick={handlerMenu} className="menu_nav_mobile">
                <i className="menu-bouner" ></i>
                <i className="menu-bouner" ></i>
                <i className="menu-bouner" ></i>
            </button>
            <ClimeDay city="bogota" country="colombia" />
            <SliderClime />
            <div className="grid-3_xs-12">
                <div className="col-3_xs-12 col-2_sm-4 col-3_md-3 col-3_lg-3">
                    <h3 className="m-0 p-5 pb-20" style={{ color: '#606060' }} ><span className="title">3 DAYS</span> <span className="subtitle">FORECAST</span></h3>
                    <CardClime data={clime} />
                </div>
                <div className="col-6_xs-12 col-6_sm-8 col-6_md-6 col-6_lg-6">
                    <AuthorClime />
                    <NewsClime />
                </div>
                <div className="col-3_xs-12 col-3_sm-4 col-3_md-3 col-3_lg-3 " style={{ position: 'relative' }}>
                    <Animated style={{zIndex: 99999, position: 'relative'}} animationIn="fadeIn" animationOut="fadeOut" animationInDuration={1000} animationOutDuration={1000} isVisible={visible}>
                        <div className="sidebar-clime">
                            <CardClimeGeneral city="paris" country="francia" />
                            <CardClimeGeneral city="madrid" country="espana" />
                            <div className="card-location">
                                <button className="btn-location" onClick={handlerClick} >Add Location</button>
                                <img src={imagelocation} style={{ width: '100%', margin: 'auto', display: 'block' }} aria-hidden alt="image clime" />
                            </div>
                        </div>
                    </Animated>
                </div>
            </div>
            <div className="copiryng">Developer for <a href="https://github.com/valenzuela21" rel="noreferrer" target="_blank">David Fernando Valenzuela Pardo</a></div>
        </div>
    </>
    );
}

export default App;
