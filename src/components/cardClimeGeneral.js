import React, {useEffect, useState} from 'react'
import {imgClime} from './helpers/imgClime';
import {helperClime} from './helpers/helperClime';
import {requestHTTP} from './helpers/requestHTTP';

export const CardClimeGeneral = ({city, country = ''}) => {

    const [clime, setClime] = useState({});

    const URL = process.env.REACT_APP_UTL_PATH;
    let token = process.env.REACT_APP_API_KEY;

    useEffect(() => {

        let pointer = `${URL}q=${city},${country}&appid=${token}`
        //Http Request Get Data
        requestHTTP(city, country, showClime, pointer)

    }, [URL, city, country, token])


    const showClime = (data) => {
        let objectClime = helperClime(data);
        setClime(objectClime);
    }

    return (
        <div className="container-general-clime">
            <div className="grid-3_xs-3 height-general-box">
                <div className="col-4_xs-4 col-4_sm-4 col-4_md-4 p-0">
                    <img src={imgClime(clime.icon)} aria-hidden className="clime-image-state" alt="image-clime"/>
                </div>
                <div
                    className="col-4_xs-4 col-4_sm-4 col-4_md-4 text-general-clime">{`${(clime.temp !== undefined) ? clime.temp : '0'} °`}</div>
                <div className="col-4_xs-4 col-4_sm-4 col-4_md-4 box-general-text">
                    <div>
                        <p className="title-card-general">{`${(clime.name !== undefined) ? clime.name : ''}`}</p>
                        <p className="subtitle-card-general">{`${(clime.country !== undefined) ? clime.country : ''}`}</p>
                    </div>
                </div>

            </div>
            <div className="grid-3_xs-1 card-details-general">
                <div
                    className="col-4 col-4_xs-4  col-4_sm-4 border-right">RH: {(clime.humadity !== undefined) ? clime.humadity : ''}</div>
                <div className="col-4 col-4_xs-4  col-4_sm-4 border-right">Max:
                    {(clime.temp_max !== undefined) ? ' ' + clime.temp_max + ' °' : '0 °'}</div>
                <div className="col-4 col-4_xs-4  col-4_sm-4">Min:
                    {(clime.temp_min !== undefined) ? ' ' + clime.temp_min + ' °' : '0 °'}</div>
            </div>
        </div>)
}
