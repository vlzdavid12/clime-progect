export const requestHTTP = (city = '', country = '', showClime = '', pointer) => {
    let xhr = new XMLHttpRequest()
    // get a callback when the server responds
    xhr.addEventListener('load', () => {
        let data = JSON.parse(xhr.responseText);
        showClime(data);
    })
    // open the request with the verb and the url
    xhr.open('GET', pointer)
    // send the request
    xhr.send();
}

export const requestForesCast = (setClime, token, URL) => {
    let xhr = new XMLHttpRequest()
    // get a callback when the server responds
    xhr.addEventListener('load', () => {
        let data = JSON.parse(xhr.responseText);
        setClime(data.list);
    })
    // open the request with the verb and the url
    xhr.open('GET', `${URL}lat=4.60&lon=-74.08&cnt=3&appid=${token}`)

    // send the request
    xhr.send();

}