export const helperClime = (data) =>{

    const {main: {temp, temp_max, temp_min, humidity}, sys: { country }, name, weather} = data;

    //Convert of Degrees
    const temp_cen = parseInt (temp - 273.15);
    const temp_max_cen = parseInt (temp_max - 273.15);
    const temp_min_cen = parseInt (temp_min - 273.15);
 

    return{'temp_min' : temp_min_cen,
    'temp_max': temp_max_cen,
    'temp': temp_cen,
    'humadity' :humidity,
    'name': name,
    'country' : country,
    'icon': weather[0].icon
    }
}