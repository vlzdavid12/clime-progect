import summer from '../../assets/images/summer.png';
import sum from '../../assets/images/sun.png';
import nigth from '../../assets/images/night.png';
import nigthLight from '../../assets/images/nightlight.png';
import cloud from '../../assets/images/cloud.png';
import rains from '../../assets/images/rain.png';
import thunders from '../../assets/images/thunders.png';
import snow from '../../assets/images/snow.png';
import wind from '../../assets/images/wind.png';
import unknown from '../../assets/images/unknown.png';

export const imgClime = (icon) => {

    let image

    switch (icon) {
        case '01d':
            image = sum
            break;
        case '01n':
            image = nigth
            break;
        case '02d':
            image = summer
            break;
        case '03d':
            image = summer
            break;
        case '03n':
            image = nigthLight
            break;
        case '04d':
            image = cloud
            break;
        case '04n':
            image = cloud
            break;
        case '09d':
            image = rains
            break;
        case '09n':
            image = rains
            break;
        case '10d':
            image = rains
            break;
        case '10n':
            image = rains
            break;
        case '11d':
            image = thunders
            break;
        case '11n':
            image = thunders
            break;
        case '13d':
            image = snow
            break;
        case '13n':
            image = snow
            break;
        case '50d':
            image = wind
            break;
        case '50n':
            image = wind
            break;
        default:
            image = unknown

    }

    return image;

}