import React from 'react';
import { imgClime } from './helpers/imgClime';
export const CardClime = ({data}) => {
    return (
        <section>
            {data.map(item=>{
                return(
                    <div className="container-list-clime" key={item.id}>
                    {/*Container*/}
                    <div className="grid-3_xs-3 ">
                        <div className="col">
                            <img className="clime-image-state p-5" src={imgClime(item.weather[0].icon)} aria-hidden alt="image-clime" />
                        </div>
                        <div className="col container-list-text-clime p-0">
                            {item.name} <br />
                            <span style={{ fontSize: '12px' }}>{item.sys.country}</span>
                        </div>
                        <div className="col">
                            <div className="number-list-clime" >
                               {`${parseInt (item.main.temp_max - 273.15)} ° / ${parseInt (item.main.temp_min - 273.15)} °`}
                   </div>
                        </div>
                    </div>
                    {/*End Container*/}

                </div>
                )
            })}

        </section>
    );
}
