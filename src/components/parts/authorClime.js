
import React from 'react';
import author1 from '../../assets/images/author/author-1.png';
import author2 from '../../assets/images/author/author-2.png';
import author3 from '../../assets/images/author/author-3.png';
export const AuthorClime = () => {

        const handelClickAuthor = () => {
                alert('Insert New Author Clime')
        }

        return (<>
                <div className="author-card grid-12_sm-12 p-left-20">
                        <div className="col-5 col-5_xs-12 col-5_sm-12 col-5_md-4 col-5_lg-4"><h2 className="m-0 txt-place subtitle"><span className="title">Place</span> to vicit</h2></div>
                        <div className="col-2 col-2_xs-3 col-2_sm-3 col-2_md-2 col-2_lg-2 text">Top Socials</div>
                        <div className="col-5 col-5_xs-8 col-5_sm-8 col-5_md-6 col-5_lg-6" style={{ display: 'flex' }} >
                                <img src={author1} className="img-author" aria-hidden alt="image-1" />
                                <img src={author2} className="img-author" aria-hidden alt="image-2" />
                                <img src={author3} className="img-author" aria-hidden alt="image-3" />
                                <button onClick={handelClickAuthor}><i className="buttom-pluss"></i></button>
                        </div>
                </div>
        </>)
}
