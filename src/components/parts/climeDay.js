import React, { useEffect, useState } from 'react';
import { imgClime } from '../helpers/imgClime';
import { helperClime } from '../helpers/helperClime';
import {requestHTTP} from '../helpers/requestHTTP';

export const ClimeDay = ({ city, country = '' }) => {

    const [clime, setClime] = useState({})

    const URL = process.env.REACT_APP_UTL_PATH;
    let token = process.env.REACT_APP_API_KEY;

    useEffect(() => {
            let pointer = `${URL}q=${city},${country}&appid=${token}`
            //Http Request Get Data
            requestHTTP(city, country, showClime, pointer)
    }, [URL, city, country, token])

    const showClime = (data) => {
        let objectClime = helperClime(data);
        setClime(objectClime);
    }

    return (
        <>
            <div className="clime-day-sidebar">
                <div className="icon-day">
                    <img src={imgClime(clime.icon)} style={{ width: '100%' }} aria-hidden alt="image-clime" />
                    <p className="m-0 p-0 txt-state">{(clime.name !== undefined) ? clime.name : ''}</p>
                </div>
                <div className="state-degrees-day">
                    {(clime.temp !== undefined) ? `${clime.temp} °` : '0°'}
                </div>
            </div>
        </>);
}
