import React from 'react';
import business from '../../assets/images/business.png'
import museum from '../../assets/images/museum.png'
import game from '../../assets/images/game.png';
export const NewsClime = () =>{

    const addNews = ( ) =>{
        alert("Attach new news")
    }

    return(
        <div className="container-news-clime p-20">
                <div className="grid-12_sm-12">
                    <div className="col-6_sm-6">
                        {/*Container Image 1*/}
                        <div className="container-1">
                            <img src={business} aria-hidden alt="image-clime" className="image-1" />
                            <div className="overlay-1" >
                            <p><i className="icon-map"></i>
                                Arabia Streat <br/>
                                Singapur
                            </p>
                            </div>
                        </div>
                         {/*End Container Image 1*/}
                        </div>  
                    <div className="col-6_sm-6">
                       {/*Container Image 2*/}
                       <div className="container-2">
                            <img src={museum} aria-hidden alt="image-clime" className="image-2" />
                            <div className="overlay-2" >
                            <p><i className="icon-map"></i>
                                Arabia Streat <br/>
                                Singapur
                            </p>
                            </div>
                        </div>
                         {/*End Container Image 2*/}
                          {/*Container Image 3*/}
                       <div className="container-3">
                            <img src={game} aria-hidden alt="image-clime" className="image-3" />
                            <div className="overlay-3" >
                            <p><i className="icon-map"></i>
                                Arabia Streat <br/>
                                Singapur
                            </p>
                            </div>
                            <button onClick={addNews} className="button-add">
                                <i className="pluss" ></i>
                            </button>
                        </div>
                         {/*End Container Image 3*/}
                    </div>
                </div>
            </div>
       
    );
}
