import React from 'react';
import './../assets/css/slider.scss'
export const SliderClime = () =>{
    return(<div className="carousel" aria-label="Gallery">
    <ol className="carousel__viewport" key={1} tabIndex={1} >
      <li id="carousel__slide1"
          className="carousel__slide"
          tabindex="carousel__slide1"
      >
        <div className="carousel__snapper">
          <a href="#carousel__slide2"
             className="carousel__prev">Go to last slide</a>
          <a href="#carousel__slide2"
             className="carousel__next">Go to next slide</a>
        </div>
      </li>
      <li id="carousel__slide2"
          className="carousel__slide"
          tabindex="carousel__slide2"
      >
        <div className="carousel__snapper">
        <a href="#carousel__slide1"
           className="carousel__prev">Go to previous slide</a>
        <a href="#carousel__slide1"
           className="carousel__next">Go to next slide</a>
           </div>
      </li>

    </ol>
    <aside className="carousel__navigation">
      <ol className="carousel__navigation-list" key={1} tabIndex={1} >
        <li className="carousel__navigation-item"  tabindex={1}>
          <a href="#carousel__slide1"
             className="carousel__navigation-button">Go to slide 1</a>
        </li>
        <li className="carousel__navigation-item"   tabindex={2}>
          <a href="#carousel__slide2"
             className="carousel__navigation-button">Go to slide 2</a>
        </li>
      </ol>
    </aside>
  </div>)
}